# Assignment

1 - calculate grade for each student and decide if they passed exam

2 - show quality of each question using 'standard psychometric values'

# 1 - Calculating a grade

- use a "Caesura"
    - used to define how a score is translated to a grade (formula?) and when a grade is marked as "passed" or "failed"
    - Assignment caesura definition:
        - Grade ranges between 1 & 10 (up to 1 decimal)
        - If score <= 20%, then grade is 1.0 & fail
        - At score 70%, grade is 5.5 & pass
        - At score 100%, grade is 10.0 & pass
    
# 2 - Analytics
    
- Create 2 statistics:
    - P'-value
    - rit-value
- See "/assets/data/Assignment.pdf" file for formulae


