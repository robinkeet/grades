# Start here

## Setup things

- Make sure PHP is locally installed
- Make sure YARN is locally installed

- In a terminal, run: 
    - `composer install`        // install project requirements
    - `yarn`                    // installs front-end things
    - `yarn encore dev`         // builds front-end package into public dir
    - `bin/console server:run`  // spins up development web-server (default usable via 127.0.0.1:8000, check CLI output)

## Classes, things & thoughts

HomeController contains calls to FileService to get data read from Excel file. Passes processed data to the templates. The choice was made to have the current logic in this function due to time constraints. This business logic would be more at home in an Assignment service.

Caesura, a model to hold the settings for an Assignment/test, such as the minimum grade, passing score, etc. Comes with sensible defaults. HomeController currently has a hard-coded (in the function) set of values for this test assignment. These values would normally be set for an Assignment (entity/object) and read in an Assignment service. 

FileService has hardcoded constants, for demo purpose, with file location and sheet name. Both are used to get the file and select a sheet within the file. File containing the data would normally be uploaded (assumption) and things like the maximum score (per question & whole) would already be available outside of the answers' sheet.

GradeService is currently static with just a single function, taking an achieved score from a student and the assignments' Caesura object (currently created in HomeController). It then uses the Caesura and the score to calculate the grade using the Caesura values.

Very light testing is done in a couple of test classes. The HomeController 'home' function is tested to ensure it returns a 200 response. The FileServices only public function is tested for expected values in the return array. The GradeService is tested using a few scores vs expected grades, though it's unsure if the actual formula used for calculations is correct. 
