<?php

namespace Test\Unit\Service;

use App\Model\Caesura;
use App\Service\GradeService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GradeServiceTest extends WebTestCase
{
    public function testGrade(): void
    {
        $caesura = new Caesura(5.5, 1, 10, 70, 20, 90);

        $gradeScores = [
            [10, 90],
            [7.8, 80],
            [5.5, 70],
            [5.4, 69],
            [2.8, 40],
            [1.9, 30],
            [1, 20],
            [1, 15],
        ];

        foreach ($gradeScores as $gradeScore) {
            $grade = $gradeScore[0];
            $score = $gradeScore[1];

            $this->assertEquals($grade, GradeService::grade($caesura, $score));
        }
    }
}
