<?php

namespace Test\Unit\Service;

use App\Service\FileService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FileServiceTest extends WebTestCase
{
    // TODO make proper test mocking providers; due to time constraints, using provided file and assuming that it's
    //  either uploaded or otherwise provided via a Provider
    public function testGetTestDataFromExcel(): void
    {
        $testResult = FileService::getTestDataFromExcel(FileService::DEMO_FILE_LOCATION, FileService::DEMO_SHEET_NAME);

        $this->assertArrayHasKey('maxScorePerQuestion', $testResult);
        $this->assertArrayHasKey('receivedAnswers', $testResult);
    }
}
