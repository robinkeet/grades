<?php

namespace Test\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testHome200Ok(): void
    {
        $url = 'http://localhost/';

        $client = $this::createClient();
        $client->request('GET', $url);
        $this->assertResponseStatusCodeSame(200, \sprintf("Expected other status code for '%s'", $url));
    }
}
