<?php

namespace App\Model;

class Caesura
{
    private float $passingGrade = 5.5;
    private float $minimumGrade = 1.0;
    private float $maximumGrade = 10.0;
    private int $passingScore = 55;
    private int $minimumScore = 10;
    private int $maximumScore = 100;

    public function __construct(
        float $passingGrade = null,
        float $minimumGrade = null,
        float $maximumGrade = null,
        int $passingScore = null,
        int $minimumScore = null,
        int $maximumScore = null
    ) {
        if ($passingGrade) {
            $this->passingGrade = $passingGrade;
        }
        if ($minimumGrade) {
            $this->minimumGrade = $minimumGrade;
        }
        if ($maximumGrade) {
            $this->maximumGrade = $maximumGrade;
        }
        if ($passingScore) {
            $this->passingScore = $passingScore;
        }
        if ($minimumScore) {
            $this->minimumScore = $minimumScore;
        }
        if ($maximumScore) {
            $this->maximumScore = $maximumScore;
        }
    }

    public function getPassingGrade(): float
    {
        return $this->passingGrade;
    }

    public function getMinimumGrade(): float
    {
        return $this->minimumGrade;
    }

    public function getMaximumGrade(): float
    {
        return $this->maximumGrade;
    }

    public function getPassingScore(): int
    {
        return $this->passingScore;
    }

    public function getMinimumScore(): int
    {
        return $this->minimumScore;
    }

    public function getMaximumScore(): int
    {
        return $this->maximumScore;
    }

}
