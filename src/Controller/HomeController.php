<?php

namespace App\Controller;

use App\Model\Caesura;
use App\Service\FileService;
use App\Service\GradeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route(name="home", path="/")
     */
    public function home(string $demoFileLocation = FileService::DEMO_FILE_LOCATION): Response
    {
        // TODO apart from file location (ie. upload), no need for this to be here -> Should be in service for assignment handling
        $testResults = FileService::getTestDataFromExcel($demoFileLocation, FileService::DEMO_SHEET_NAME);

        $maxAchievableScore = \array_sum($testResults['maxScorePerQuestion']);

        $caesura = new Caesura(5.5, 1, 10, 70, 20, $maxAchievableScore);

        // TODO Should be moved to some "Assignment service" for handling test setup & results -> running out of time
        foreach ($testResults['receivedAnswers'] as $student => $data) {

            // calculate score percentage
            $studentScore = round(\array_sum($data['answers']) / $maxAchievableScore * 100, 0);
            $testResults['receivedAnswers'][$student]['score'] = $studentScore;

            // calculate grade using caesura
            $testResults['receivedAnswers'][$student]['grade'] = GradeService::grade($caesura, $studentScore);
        }

        return $this->render(
            'pages/home.html.twig',
            [
                'testResults' => $testResults,
                'caesura'     => $caesura,
            ]
        );
    }
}
