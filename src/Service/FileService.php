<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * TODO create separate provider to remove binding of external vendor from business logic
 */
class FileService
{
    // TODO These should not exist - for demo/assignment purposes only
    public const DEMO_FILE_LOCATION = '/build/data/Assignment.xlsx';
    public const DEMO_SHEET_NAME    = 'Sheet0';

    /**
     * // TODO have Service handling getting of Spreadsheet with separate Providers, in this case a Provider using the
     *      PhpSpreadsheet vendor
     * // TODO exception handling (file not found, rights, etc.)
     */
    private static function getSpreadsheet(string $location): Spreadsheet
    {
        $location = \getcwd() . $location;
        // TODO this must be removed - here to change the route for cli (phpunit)
        if (PHP_SAPI === 'cli') {
            $location = \getcwd() . '/assets/data/Assignment.xlsx';
        }

        return IOFactory::load($location);
    }

    /**
     * // TODO assuming template of Excel sheet:
     *          - first row is title row (ID/Score Question 1/Score Question 2/ etc.)
     *          - second row is Max score per question
     *          - onward score achieved by student per question
     *          - first column = student name
     * // TODO make sure the above assumptions are somehow set -> out of scope for now
     *
     * Reads the data from Excel file from file location & sheet name
     */
    public static function getTestDataFromExcel(string $fileLocation, string $sheetName): array
    {
        // The logic of determining title rows, how to return the data, etc. should be out of scope of a
        // file handling service. It is done here due to time constraints. Would be better in an Assignment service,
        // which would receive just the data in bulk (1 array). That way a "FileService" would be re-usable for more
        // than just Excel sheets with this data format.

        $titleRow                = 1;
        $maxScoreRow             = 2;
        $maxScorePerQuestion     = [];
        $studentScorePerQuestion = [];

        $spreadsheet = self::getSpreadsheet($fileLocation);
        $sheet       = $spreadsheet->getSheetByName($sheetName);

        foreach ($sheet->getRowIterator() as $currentRow => $row) {
            if ($currentRow === $titleRow) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $studentName = '';

            foreach ($cellIterator as $currentColumn => $cell) {

                // if current row is "max score" defining row, set max scores array and skip setting first column
                if ($currentRow === $maxScoreRow) {
                    if ($currentColumn === 'A') {
                        continue;
                    }

                    // set scores in max score array
                    $maxScorePerQuestion[$currentColumn] = $cell->getValue();
                    continue;
                }

                // set student name outside of foreach, use to extract scores
                if ($currentColumn === 'A') {
                    $studentName = $cell->getValue();
                    continue;
                }

                $studentScorePerQuestion[$studentName]['answers'][$currentColumn] = $cell->getValue();
            }
        }

        return [
            'maxScorePerQuestion' => $maxScorePerQuestion,
            'receivedAnswers'     => $studentScorePerQuestion,
        ];
    }
}
