<?php

namespace App\Service;

use App\Model\Caesura;

class GradeService
{
    private const GRADE_DECIMAL_POINTS = 1;

    /**
     * TODO no idea if this is correct, could not find formula. So math'ed it myself :) TODO -> check correctness
     */
    public static function grade(Caesura $caesura, float $studentScore): float
    {
        $rounded = (int) round($studentScore, 0);

        if ($rounded === $caesura->getMaximumScore()) {
            return $caesura->getMaximumGrade();
        }

        if ($rounded >= $caesura->getPassingScore()) {
            $passPointsAvailable = $caesura->getMaximumGrade() - $caesura->getPassingGrade();
            $passPointValue      = $passPointsAvailable / ($caesura->getMaximumScore() - $caesura->getPassingScore());

            return round(
                $caesura->getPassingGrade() + ($passPointValue * ($studentScore - $caesura->getPassingScore())),
                self::GRADE_DECIMAL_POINTS
            );
        }

        if ($rounded >= $caesura->getMinimumScore()) {
            $failPointsAvailable = $caesura->getPassingGrade() - $caesura->getMinimumGrade();
            $failPointsValue     = $failPointsAvailable / ($caesura->getPassingScore() - $caesura->getMinimumScore());

            return round(
                $caesura->getMinimumGrade() + ($failPointsValue * ($studentScore - $caesura->getMinimumScore())),
                self::GRADE_DECIMAL_POINTS
            );
        }

        return $caesura->getMinimumGrade();
    }
}
